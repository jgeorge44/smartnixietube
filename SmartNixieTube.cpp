/* Driver for Smart Nixie Tube Kit by Switchmode Design by Tyler Nehowig
 * (http://switchmodedesign.com/collections/arduino-shields/products/smart-nixie-tube)
 *
 * Version 1.0 2014-04-21 Initial release
 *
 * This software is released under the MIT License, which reads:
 
 Copyright (C) 2014 Joe George, jgeorge@nbi.com

 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights to
 use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
 the Software, and to permit persons to whom the Software is furnished to do so,
 subject to the following conditions:

 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
 INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
 PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF
 CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
 OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

 * The actual Nixie modules run SmartNixieTubeFirmware which
 * listens on the serial port for a command string.
 * 
 * This application runs on (yet another) Arduino type module, and
 * drives the tubes from a SoftwareSerial port defined to talk to them.
 *
 * This driver provides an abstraction layer to the Smart Nixie Tube serial protocol,
 * for ease of development.
 *
 * To connect this to a Smart Nixie Tube string, connect three wires to the "input" header
 * on the left side of the top board of the leftmost Nixie Tube in your setup:
 * Pin 1 is the "frontmost" pin on the connector, closest to the Nixie Tube itself
 * Pin 1+2 (GND) to Arduino GND
 * Pin 3 (RX IN) to Arduino Digital Pin 10
 * Pin 4+5 (VIN) to Arduino VIN (the one before the regulator)
 *
 */

#include <SmartNixieTube.h>
#include <SoftwareSerial.h>

SoftwareSerial* nixiePort;
NixieTube seriesOfTubes[NUM_TUBES];

// Constructor
SmartNixieTube::SmartNixieTube(int rxPin, int txPin) {
  nixiePort = new SoftwareSerial(rxPin, txPin);
  nixiePort->begin(115200);
}

void SmartNixieTube::setRGB(byte r, byte g, byte b) {
  // Set Accent LED RGB values
  for (int i=0; i<NUM_TUBES; i++) {
    setRGB(i, r, g, b);
  }
}

void SmartNixieTube::setRGB(int which, byte r, byte g, byte b) {
  // Set Accent LED RGB values
  seriesOfTubes[which].red = r;
  seriesOfTubes[which].green = g;
  seriesOfTubes[which].blue = b;
}

void SmartNixieTube::setBrightness(byte level) {
  // Set PWM value for Nixie tube brightness (255=max)
  for (int i=0; i<NUM_TUBES; i++) {
    setBrightness(i, level);
  }
}  

void SmartNixieTube::setBrightness(int which, byte level) {
  // Set PWM value for Nixie tube brightness (255=max)
  seriesOfTubes[which].brightness = level;
}

void SmartNixieTube::setDecimal(int which, boolean lhdp, boolean rhdp) {
  // which = which tube in the string ( 0<=which<NUM_TUBES)
  if (which < NUM_TUBES) { 
    seriesOfTubes[which].lhdp = lhdp;
    seriesOfTubes[which].rhdp = rhdp;
  }
}
  
void SmartNixieTube::setDigit(int which, char value) {
  if (which < NUM_TUBES) { 
    seriesOfTubes[which].digit = value;
  }
}

void SmartNixieTube::outputTubeSerial(int which) {
  // Take a struct outlining a specific tube and output the serial protocol
  // needed to set it's state.
  char buffer[23];
  buffer[0] = '$';
  buffer[1] = seriesOfTubes[which].digit;
  buffer[2] = ',';
  buffer[3] = (seriesOfTubes[which].lhdp ? 'Y' : 'N');
  buffer[4] = ',';
  buffer[5] = (seriesOfTubes[which].rhdp ? 'Y' : 'N');
  buffer[6] = ',';
  // brightness 7-9
  buffer[7] = '0';
  buffer[8] = '0';
  buffer[9] = '0';
  if (seriesOfTubes[which].brightness > 99) {
    buffer[7] = seriesOfTubes[which].brightness / 100 + '0';
  }
  if (seriesOfTubes[which].brightness > 9) {
    buffer[8] = (seriesOfTubes[which].brightness / 10) % 10 + '0';
  }
  buffer[9] = seriesOfTubes[which].brightness % 10 + '0'; 
  buffer[10] = ',';
  // red 11-13  
  buffer[11] = '0';
  buffer[12] = '0';
  buffer[13] = '0';
  if (seriesOfTubes[which].red > 99) {
    buffer[11] = seriesOfTubes[which].red / 100 + '0';
  }
  if (seriesOfTubes[which].red > 9) {
    buffer[12] = (seriesOfTubes[which].red / 10) % 10 + '0';
  }
  buffer[13] = seriesOfTubes[which].red % 10 + '0'; 
  buffer[14] = ',';
  // green 15-17
  buffer[15] = '0';
  buffer[16] = '0';
  buffer[17] = '0';
  if (seriesOfTubes[which].green > 99) {
    buffer[15] = seriesOfTubes[which].green / 100 + '0';
  }
  if (seriesOfTubes[which].green > 9) {
    buffer[16] = (seriesOfTubes[which].green / 10) % 10 + '0';
  }
  buffer[17] = seriesOfTubes[which].green % 10 + '0'; 
  buffer[18] = ',';
  // blue 19-21
  buffer[19] = '0';
  buffer[20] = '0';
  buffer[21] = '0';
  if (seriesOfTubes[which].blue > 99) {
    buffer[19] = seriesOfTubes[which].blue / 100 + '0';
  }
  if (seriesOfTubes[which].blue > 9) {
    buffer[20] = (seriesOfTubes[which].blue / 10) % 10 + '0';
  }
  buffer[21] = seriesOfTubes[which].blue % 10 + '0'; 
  buffer[22] = 0x0; //null
  
  //Serial.println(buffer);
  nixiePort->println(buffer);
}  

void SmartNixieTube::dump() {
  dump(false);
}

void SmartNixieTube::dump(boolean more) {
  // As a convenience, dump the nixie tube buffer to the Serial port
  // FIXME wrap this in a define for boards that don't have a serial port
  // like ATTiny
  if (more) Serial.println("----- dump() -----");

  if (more) {
    Serial.print("Digit     : ");
  } else {
    Serial.print("dump: ");
  }
  for (int i=0; i<NUM_TUBES; i++) {
    Serial.print(seriesOfTubes[i].lhdp ? "." : " ");
    Serial.print(seriesOfTubes[i].digit);
    Serial.print(seriesOfTubes[i].rhdp ? "." : " ");
    if (more) Serial.print(" ");
  }
  Serial.println();

  if (!more) return;

  Serial.print("Brightness: ");
  for (int i=0; i<NUM_TUBES; i++) {
    if (seriesOfTubes[i].brightness < 100) Serial.print(" ");
    if (seriesOfTubes[i].brightness < 10) Serial.print(" ");
    Serial.print(seriesOfTubes[i].brightness, DEC);
    Serial.print(" ");
  }
  Serial.println();

  Serial.print("RGB Red   : ");
  for (int i=0; i<NUM_TUBES; i++) {
    if (seriesOfTubes[i].red < 100) Serial.print(" ");
    if (seriesOfTubes[i].red < 10) Serial.print(" ");
    Serial.print(seriesOfTubes[i].red, DEC);
    Serial.print(" ");
  }
  Serial.println();

  Serial.print("RGB Green : ");
  for (int i=0; i<NUM_TUBES; i++) {
    if (seriesOfTubes[i].green < 100) Serial.print(" ");
    if (seriesOfTubes[i].green < 10) Serial.print(" ");
    Serial.print(seriesOfTubes[i].green, DEC);
    Serial.print(" ");
  }
  Serial.println();

  Serial.print("RGB Blue  : ");
  for (int i=0; i<NUM_TUBES; i++) {
    if (seriesOfTubes[i].blue < 100) Serial.print(" ");
    if (seriesOfTubes[i].blue < 10) Serial.print(" ");
    Serial.print(seriesOfTubes[i].blue, DEC);
    Serial.print(" ");
  }
  Serial.println();
}

void SmartNixieTube::clearBuffer() {
  // turn all tubes off
  for (int i=0; i<NUM_TUBES; i++) {
    setDigit(i, '-');
    setDecimal(i,false,false);
  }
}

void SmartNixieTube::clear() {
  // clear buffer
  clearBuffer();
  // then clear the display as well
  nixiePort->println("@");
}

long SmartNixieTube::tenToThe(int power) {
  // hacky simple routine to determine 10^power
  if (power==0)
    return 1;
  return (long)(10 * tenToThe(power-1));
}

void SmartNixieTube::show() {
  // The base, no-parameter show() will just output the contents of the
  // tube array, assuming they've been set properly already
  // either by one of the overloaded show() methods, or by setDigit/setDecimal)

  // The Smart Nixie Tube string acts a a left-to-right shift register when
  // serial commands are sent down the line. For example, with 4 tubes in a string,
  // sending a command to set a single tube to "1" will set the leftmost digit to
  // 1 ("1---"). However, sending the serial command to display a "2" next will
  // actually shift the "1" DOWN the line, and the display will show "21--" instead
  // of the "12--" which you might expect. In short, this means that in order to
  // display a series of digits such as "1 2 3 4", we must actually send the serial
  // commands in reverse order ("4 3 2 1") in order to get what we want. The easiest
  // way to manage this reversal is simply to output the serial commands to set the tubes
  // in the reverse order from the way we logically think of them.

#ifdef DEBUG
  dump(false);
#endif
  for (int i=NUM_TUBES-1; i>=0; i--) {
    outputTubeSerial(i);
  }
  nixiePort->println("!");
}

void SmartNixieTube::show(long value) {
  // overload the most common show() usage to default to no leading zero
  show(value, false);
}

void SmartNixieTube::show(long value, boolean wantLeadingZeroes) {
  // Set the digit string all at once to a numeric value
  // numbers too large for the number of tubes will have their most significant
  // digits cut off (i.e, show(123456) with 4 tubes will show "3456")

  // We don't clear the tube buffer here, so you can use this function to 
  // do the math to parse an integer and still manually manipulate
  // decimal points and whatnot (like show(float) does)

  long power = tenToThe(NUM_TUBES-1);
  int position = 0;
  long number = 0;

  while (power > 0) {
    if (value >= power) {
      number = value / power;
      setDigit(position, number % 10 + '0');
      value-=(number * power);
      wantLeadingZeroes = true;
    } else {
      if (wantLeadingZeroes) setDigit(position, '0');
    }
    position++;
    power=power/10;
  }
  show();
}

void SmartNixieTube::show(float value) {
  show((double)value);
}

void SmartNixieTube::show(double value) {
  // Twiddle the digits and decimal points to show a double value
  // FIXME really big floats and doubles will round (12345678.9 displays as 12345679)
  // FIXME float and double don't work with NUM_TUBES>9

  clearBuffer();

  // determine the integer portion of the double
  int power = 0;
  while ((long) value >= tenToThe(power)) {
    power++;
  }

  if (power >= NUM_TUBES) {
    // If the integer portion is larger than the display just show it with a trailing dot
    // The trailing dot is different than just showing it as a long integer
    setDecimal(NUM_TUBES-1, false, true);
    show((long)value, false);
    return;
  }

  // If there are trailing zeroes after the decimal point, drop them
  while (power<NUM_TUBES-1 && (long)(value * tenToThe(NUM_TUBES-power)) % 10 == 0) {
    //Serial.println((long)(value * tenToThe(NUM_TUBES-power)));
    power++;
  }

  // If power <=0 the passed value is less than 1, so we want to set a single
  // 0 before the decimal point to format it properly
  if (power <= 0) power = 1;

  if (power > 0) {
    // shift our decimal point by multiplying by the power of 10 we need
    // then manually set a decimal point
    setDecimal(power, true, false);
    // The loop below "fills in" leading zeroes for numbers like 0.0001
    for (int i=power-1; i<NUM_TUBES; i++) {
      setDigit(i, '0');
    }
    show((long)(value * tenToThe(NUM_TUBES-power)), false);
  }
}

/* end SmartNixieTube.cpp */
