
/* Driver for Smart Nixie Tube Kit
 *
 * This software is released under the MIT License, which reads:
 
 Copyright (C) 2014 Joe George, jgeorge@nbi.com

 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights to
 use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
 the Software, and to permit persons to whom the Software is furnished to do so,
 subject to the following conditions:

 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
 INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
 PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF
 CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
 OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

 */
 
/*
 * The actual Nixie modules run SmartNixieTubeFirmware which
 * listens on the serial port for a command string.
 * 
 * This application runs on (yet another) Arduino type module, and
 * drives the tubes from a SoftwareSerial port defined to talk to them.
 *
 * To connect this to a Smart Nixie Tube string, connect three wires to the "input" header
 * on the top left side of the leftmost Nixie Tube in your setup:
 * Pin 1 is the "frontmost" pin on the connector, that is, closest to the Nixie Tube itself
 * Pin 1+2 (GND) to Arduino GND
 * Pin 3 (RX IN) to Arduino Digital Pin 10
 * Pin 4+5 (+9V DC Common) to Arduino VIN (the one before the regulator)
 *
 */

#include <SoftwareSerial.h>
#include <SmartNixieTube.h>

// IMPORTANT: set NUM_TUBES in SmartNixieTube.h to the number of tubes you have!

const int TX = 10;   // TX Pin for SoftwareSerial
const int RX = 11;   // not connected to anything but SoftwareSerial needs an RX pin defined

SmartNixieTube nixie = SmartNixieTube(RX, TX);

void setup() {                
  // Console serial port
  Serial.begin(115200);

  // Generate random seed
  randomSeed(analogRead(0));

}

void loop() {
  // Max brightness for Nixie Tubes
  nixie.setBrightness(255);
  // Set accent color
  nixie.setRGB(255, 0, 255);  

  // You can easily display integers
  // without leading zeroes
  nixie.clear();
  nixie.show(123456);
  delay(1000);
  // without leading zeroes
  nixie.clear();
  nixie.show(42, false);
  delay(1000);
  // WITH leading zeroes
  nixie.clear();
  nixie.show(4242, true);
  delay(1000);

  // You can display floating point values easily
  nixie.show(0.001f);
  delay(1000);
  nixie.show(3.1415926f);
  delay(1000);
  nixie.show(99.9f);
  delay(1000);
  
  // Setting accent LED colors, one at a time, or all of them
  nixie.setRGB(0,0,0);
  nixie.setRGB(0,255,255,255);
  nixie.setRGB(5,255,255,255);
  // Clears anything stored in the buffer (show(float) clears before it displays, but
  // none of the other show() methods do (so you only can only update a single tube/etc)
  nixie.clear();
  // Set individual digits as needed too
  nixie.setDigit(0,'1');
  nixie.setDigit(1,'1');
  nixie.setDigit(4,'1');

  // Set left and right hand decimal places individually  
  nixie.setDecimal(0,false,true);
  nixie.setDecimal(1,true,false);
  nixie.setDecimal(4,true,true);
  // show() just shows what's in the buffer if you've set anything manually, like above
  nixie.show();
  delay(1000);

  // You can set RGB colors only if needed (either all at once, or individual tubes)
  nixie.clear();
  nixie.show(654321);
  for (int i=0; i<16; i++) {
    nixie.setRGB(random(255),random(255),random(255));
    nixie.setBrightness(255);
    nixie.show();
    delay(200);
  }
  nixie.setRGB(0,0,0);
  
  // set brightness only if needed (all at once, or individual tubes)  
  for (int i=16; i<256; i+=16) {
    nixie.setBrightness(i);
    nixie.show();
    delay(200);
  }

  nixie.setBrightness(90);
  nixie.setBrightness(0,255);
  nixie.setBrightness(1,255);
  nixie.setBrightness(4,255);
  nixie.show();
  delay(2000);
  
  // You can also update the tube string reasonably quickly and it'll keep up
  nixie.clear();
  nixie.setBrightness(255);
  nixie.setRGB(255,0,0);
  for (int i=0; i<200; i++) {
    nixie.show(random(100000,1000000), false);
  }
}

