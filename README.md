Driver for Smart Nixie Tube Kit by Switchmode Design by Tyler Nehowig
(http://switchmodedesign.com/collections/arduino-shields/products/smart-nixie-tube)

Copyright (C) 2014 Joe George

This software is released under the MIT License. See LICENSE for details.
 
The actual Nixie modules run SmartNixieTubeFirmware which
listens on the serial port for a command string.

This application runs on (yet another) Arduino type module, and
drives the tubes from a SoftwareSerial port defined to talk to them.

This driver provides an abstraction layer to the Smart Nixie Tube serial protocol,
for ease of development.

To connect this to a Smart Nixie Tube string, connect three wires to the "input" header
on the left side of the top board of the leftmost Nixie Tube in your setup:
Pin 1 is the "frontmost" pin on the connector, closest to the Nixie Tube itself
Pin 1+2 (GND) to Arduino GND
Pin 3 (RX IN) to Arduino Digital Pin 10
Pin 4+5 (VIN) to Arduino VIN (the one before the regulator)


To install this library in your Arduino (>1.0) SDK, click the "Download ZIP"
button to the right. Download the ZIP file and uncompress it. The ZIP file will
uncompress into a folder named "SmartNixieTube-master". Rename this folder to
"SmartNixieTube" and install it in your Arduino libraries folder (see
http://arduino.cc/en/Guide/Libraries under "Manual Installation" for instructions.)


