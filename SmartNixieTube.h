/* Driver for Smart Nixie Tube Kit by Switchmode Design by Tyler Nehowig
 * (http://switchmodedesign.com/collections/arduino-shields/products/smart-nixie-tube)
 *
 * Version 1.0 2014-04-21 Initial release
 *
 * This software is released under the MIT License, which reads:

 Copyright (C) 2014 Joe George, jgeorge@nbi.com

 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights to
 use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
 the Software, and to permit persons to whom the Software is furnished to do so,
 subject to the following conditions:

 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
 INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
 PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF
 CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
 OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

 */

#ifndef _SmartNixieTube_H
#define _SmartNixieTube_H_
#include <Arduino.h>

// Change this define to the number of tubes in your Smart Nixie Tube setup
#define NUM_TUBES 6
// FIXME show(float) and show(double) don't work with more than 9 tubes

// setting this define will set the library to do some debugging while it's working
// most notably, any call to show() will dump the displayed digits out to Serial
#undef DEBUG
//#define DEBUG 1

// This struct is everything we need to know about setting a single Smart Nixie Tube
// status. The serial protocol is all ASCII and with zero-padded values, the struct
// doesn't care because the output routine will do the proper protocol formatting
struct NixieTube {
  char digit;      // Digit is 0-9, obviously, or '-' to turn off the tube (show nothing)
  boolean lhdp;    // Left-hand decimal point on or off
  boolean rhdp;    // Right-hand decimal point on or off
  byte brightness; // Brightness of Nixie Tube, 0-255 (PWM driven)
  byte red;        // RGB LED Red component (0-255)
  byte green;      // RGB LED Green component (0-255)
  byte blue;       // RGB LED Blue component (0-255)
};

class SmartNixieTube {
        public:
        
            // constructor
            SmartNixieTube(int rxPin, int txPin);

            // Set Accent LED Color - "which" sets a single tube, other method sets all tubes
            void setRGB(byte r, byte g, byte b);
            void setRGB(int which, byte r, byte g, byte b);
            
            // Set Nixie Tube Brightness - "which" sets a single tube, other method sets all tubes
            void setBrightness(byte level);
            void setBrightness(int which, byte level);

            // Set Decimal Points on individual tubes in the array - "which" sets a single tube
            void setDecimal(int which, boolean lhdp, boolean rhdp);

            // Set an individual digit in the array - "which" sets a single tube
            void setDigit(int which, char value);

            // Clear the tube display
            void clear();

            // Display the contents of the tube array (if set via setDigit and setDecimal)
            void show();

            // Display a particular numeric value
            void show(long value, boolean wantLeadingZeroes);
            void show(long value);
            void show(float value);
            void show(double value);

            // dump Nixie array to console serial
            void dump();
            void dump(boolean more);

        private:
            void clearBuffer();
            void outputTubeSerial(int which);
            long tenToThe(int power);
};

#endif // _SmartNixieTube_H_
